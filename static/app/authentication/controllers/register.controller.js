/**
 * Register controller
 * @namespace app.authentication.controllers
 */
(function () {
    'use strict';

    angular
        .module('app.authentication.controllers')
        .controller('RegisterController', RegisterController);

    RegisterController.$inject = ['$location', '$scope', 'Authentication'];

    /**
     * @namespace RegisterController
     */
    function RegisterController ($location, $scope, Authentication) {
        var vm = this;

        vm.register = register;
        vm.errorMessage = null;

        activate();

        /**
         * @name active
         * @desc Action to be performed when this controller is instantiated
         */
        function activate() {
            if(Authentication.isAuthenticated()) {
                $location.url('/');
            }
        }

        /**
         * @name register
         * @desc Register a new user
         * @memberOf app.authentication.controllers.RegisterController
         */
        function register() {
            Authentication.register(vm.username, vm.password, vm.confirmPassword, vm.email)
                .error(onRegisterError);

            function onRegisterError (data) {
                vm.errorMessage = data.message;
            }
        }
    }
})();