/**
 * Authentication
 * @namespace app.authentication.services
 */
(function () {
    'use strict';

    angular.module('app.authentication.services')
        .factory('Authentication', Authentication);

    Authentication.$inject = ['$cookies', '$http', '$location'];

    /**
     * @namespace Authentication
     * @returns {Factory}
     */
    function Authentication($cookies, $http, $location) {
        /**
         * @name Authentication
         * @desc The Factory to be returned
         */
        var that = {
            login: login,
            logout: logout,
            register: register,
            refreshAccount: loadAuthenticatedAccount,
            getAccount: getAuthenticatedAccount,
            getToken: getAuthenticatedToken,
            isAuthenticated: isAuthenticated
        },
            email_enable = false;

        active();

        return that;

        ///////////////////////

        function active () {
            if (isAuthenticated()) {
                loadAuthenticatedAccount()
                    .error(function () {
                        unauthenticate();
                    });
            }
        }

        /**
         * @name login
         * @desc Try to log in with given email and password
         * @param {string} email The email entered by the user
         * @param {string} password The password entered by the user
         * @returns {HttpPromise}
         * @memberOf app.authentication.services.Authentication
         */
        function login(username, password) {
            return $http
                .post('/api/v1/auth/login/', {
                    username: username, password:password
                })
                .success(function(data) {
                    setAuthenticationToken(data.key);
                    loadAuthenticatedAccount()
                        .success(function(){
                            $location.path('/');
                        });
                })
                .error(function(data) {
                    //TODO handle when login failed
                    console.error(data);
                });
        }

        /**
         * @name logout
         * @desc Log the user out
         * @returns {HttpPromise}
         * @memberOf app.authentication.services.Authentication
         */
        function logout() {
            return $http
                .post('api/v1/auth/logout/',{})
                .success(function() {
                    unauthenticate();
                    $location.path('/');
                })
                .error(function(data) {
                    console.error(data);
                });
        }

        /**
         * @name register
         * @desc Try to register a new user
         * @param {string} email The email entered by user
         * @param {string} password The password entered by user
         * @param {string} confirmPassword The confirm password entered by user
         * @param {string} username The username entered by user
         * @returns {HttpPromise}
         * @memberOf app.authentication.services.Authentication
         */
        function register(username, password, confirmPassword, email) {
            return $http
                .post('/api/v1/auth/registration/', {
                    username: username,
                    password1: password,
                    password2: confirmPassword,
                    email: email || null
                })
                .success(function(data) {
                    if (!email_enable){
                        $http.post('/api/v1/auth/registration/verify-email/', {key: data.key})
                            .success(function(){
                                login(username, password);
                            });
                    } else {
                        //TODO Redirect to waiting for confirm email page
                    }
                })
                .error(function(data) {
                    //TODO handle register failure
                    console.error(data);
                });
        }

        /**
         * @name loadAuthenticatedAccount
         * @desc load authenticated account data
         * @returns {promise}
         * @memberOf app.authentication.services.Authentication
         */
        function loadAuthenticatedAccount() {
            return $http.get('/api/v1/auth/user/')
                .success(function (data) {
                    $cookies.putObject('authenticatedAccount', data);
                })
                .error(function() {
                    unauthenticate();
                    $location.path('/');
                });
        }

        /**
         * @name getAuthenticatedAccount
         * @desc Return the currently authenticated account
         * @returns {object|undefined} Account if authenticated, else 'null'
         * @memberOf app.authentication.services.Authentication
         */
        function getAuthenticatedAccount() {
            if(!$cookies.getObject('authenticatedAccount')) {
                return null;
            }
            return $cookies.getObject('authenticatedAccount');
        }

        /**
         * @name setAuthenticationToken
         * @desc Stringify the account object and store it in a cookie
         * @param {Object} account The account object to be stored
         * @memberOf app.authentication.services.Authentication
         */
        function setAuthenticationToken(key) {
            $cookies.put('key', key);
        }
        
        /**
         * @name getAuthenticatedToken
         * @desc Return the currently authenticated account
         * @returns {string|undefined} Key if authenticated, else 'null'
         * @memberOf app.authentication.services.Authentication
         */
        function getAuthenticatedToken() {
            if(!$cookies.get('key')) {
                return null;
            }
            return $cookies.get('key');
        }

        /**
         * @name isAuthenticated
         * @desc Check if the current user is authenticated
         * @returns {boolean} True if the user is authenticated, else false
         * @memberOf app.authentication.services.Authentication
         */
        function isAuthenticated() {
            return !!$cookies.get('key');
        }

        /**
         * @name unauthenticate
         * @desc Delete the cookie where the user object is stored
         * @memberOf app.authentication.services.Authentication
         */
        function unauthenticate() {
            var cookies = $cookies.getAll();
            angular.forEach(cookies, function (v, k) {
                $cookies.remove(k);
            });
        }
    }

})();