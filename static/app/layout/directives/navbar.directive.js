/**
* sidebar
* @namespace app.layout.directives
*/
(function () {
  'use strict';

  angular
    .module('app.layout.directives')
    .directive('navbar', navbar);

  /**
  * @namespace navbar
  */
  function navbar() {
    /**
    * @name directive
    * @desc The directive to be returned
    * @memberOf app.layout.directives.navbar
    */
    var directive = {
      controller: 'NavbarController',
      controllerAs: 'vm',
      restrict: 'E',
      //scope: {
      //  sidebar: '='
      //},
      templateUrl: '/static/app/layout/templates/navbar.html'
    };

    return directive;
  }
})();