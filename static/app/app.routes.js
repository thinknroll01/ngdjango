/**
 * ngRoute configure
 * @namespace app.routes
 */
(function () {
    'use strict';

    angular
        .module('app.routes')
        .config(config);

    config.$inject = ['$stateProvider', '$urlRouterProvider'];

    /**
     * @name config
     * @desc Define valid application routes
     */
    function config($stateProvider, $urlRouterProvider) {
        $stateProvider
            .state('home', {
                url: '/',
                controller: 'IndexController',
                controllerAs: 'vm',
                templateUrl: '/static/app/layout/templates/index.html'
            })
            .state('register', {
                url: '/register',
                controller: 'RegisterController',
                controllerAs: 'vm',
                templateUrl: '/static/app/authentication/templates/register.html'
            })
            .state('login', {
                url: '/login',
                controller: 'LoginController',
                controllerAs: 'vm',
                templateUrl: '/static/app/authentication/templates/login.html'
            });
        $urlRouterProvider.when("", "/");
        $urlRouterProvider.otherwise('/');
    }

})();