"""ngDjango URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.8/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Add an import:  from blog import urls as blog_urls
    2. Add a URL to urlpatterns:  url(r'^blog/', include(blog_urls))
"""
from rest_framework_swagger.views import get_swagger_view
from django.conf.urls import include, url
from django.contrib import admin
from ngdjango.views import IndexView

schema_view = get_swagger_view(title='ngDjango API')

urlpatterns = [
    url(r'^admin/', include(admin.site.urls)),
    url(r'^api/docs/', schema_view),
    url(r'^api/v1/auth/', include('rest_auth.urls')),
    url(r'^api/v1/auth/registration/', include('rest_auth.registration.urls')),
    # url(r'^api/v1/', include('')),
    url(r'^.*$', IndexView.as_view())
]
