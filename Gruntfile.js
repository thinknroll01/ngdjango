'use strict';

module.exports = function (grunt) {

  var index = 'templates/index.html';

  // Time how long tasks take. Can help when optimizing build times
  require('time-grunt')(grunt);

  // require all grunt tasks
  require('jit-grunt')(grunt, {
    replace: 'grunt-text-replace'
  });

  // Define the configuration for all the tasks
  grunt.initConfig({

    // Watches files for changes and runs tasks based on the changed files
    watch: {
      options: {
        livereload: true
      },
      templates: {
        files: ['static/app/templates/**/*.html'],
        tasks: ['newer:all']
      },
      bower: {
        files: ['bower.json'],
        tasks: ['wiredep', 'replace:after_wiredep']
      },
      js: {
        files: ['static/app/**/*.js'],
        tasks: ['newer:jshint:all', 'angularFileLoader:server']
      },
      sass: {
        files: ['static/styles/**/*.{scss,sass}'],
        tasks: ['sass:server', 'autoprefixer:server', 'injector:css']
      },
      gruntfile: {
        files: ['Gruntfile.js']
      }
    },

    // Make sure code styles are up to par and there are no obvious mistakes
    jshint: {
      options: {
        jshintrc: '.jshintrc',
        reporter: require('jshint-stylish')
      },
      all: {
        src: [
          'Gruntfile.js',
          'static/app/**/*.js'
        ]
      }
    },

    // Automatically inject Bower components into the app
    wiredep: {
      app: {
        src: [index],
        ignorePath:  /\.\.\//
      }
    },

    replace: {
      after_wiredep: {
        src: [index],
        overwrite: true,                 // overwrite matched source files
        replacements: [{
          from: /bower_components/g,
          to: "static"
        }]
      }
    },

    angularFileLoader: {
      options: {
        scripts: ['static/app/**/*.js'],
        relative: false
      },
      server: {
        src: [index]
      }
    },

    // Compiles Sass to CSS and generates necessary files if requested
    sass: {
      options: {
        sourceMap: true
      },
      server: {
        files: {
          'static/styles/main.css': 'static/styles/main.scss'
        }
      }
    },

    // Add vendor prefixed styles
    autoprefixer: {
      options: {
        browsers: ['last 2 version']
      },
      server: {
        options: {
          map: false
        },
        files: [{
          expand: true,
          cwd: 'static/styles',
          src: '**/*.css',
          dest: 'static/styles'
        }]
      }
    },

    injector: {
      options: {},
      css: {
        files: {
          index: ['static/styles/**/*.css']
        }
      }
    }
  });

  grunt.registerTask('default', 'Compile', function (target) {

    grunt.task.run([
      'wiredep',
      'replace:after_wiredep',
      'newer:jshint:all',
      'angularFileLoader:server',
      'sass:server',
      'autoprefixer:server',
      'injector:css'
    ]);
  });
  
  grunt.registerTask('serve', 'Compile then start a connect web server', function (target) {

    grunt.task.run([
      'wiredep',
      'replace:after_wiredep',
      'newer:jshint:all',
      'angularFileLoader:server',
      'sass:server',
      'autoprefixer:server',
      'injector:css',
      'watch'
    ]);
  });

};
